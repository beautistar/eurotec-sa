import React, { Component, useEffect } from 'react';
import { KeyboardAvoidingView, TouchableOpacity, BackHandler, ToastAndroid } from 'react-native';
import {
  Scene,
  Router,
  Actions,
  Drawer,
} from 'react-native-router-flux';
import { Icon, Right, Left } from 'native-base'
import { Color, Dimensions } from '../helper'
import Welcome from '../screens/signin';
import Intro from '../screens/signin/Intro';
import SignUp from '../screens/signup';
import SignIn from '../screens/signin/SignIn';
import Forgot from '../screens/signin/Forgot';
import ChangePassword from '../screens/signin/ChangePassword';
import Guest from '../screens/guest';
import IntroGuest from '../screens/guest/IntroGuest';
import Accessories from '../screens/guest/accessories'
import Blum from '../screens/guest/accessories/Blum'
import Vibo from '../screens/guest/accessories/Vibo'
import Vauth_Sagel from '../screens/guest/accessories/Vauth_Sagel'
import System_Holz from '../screens/guest/accessories/System_Holz'
import Fop from '../screens/guest/accessories/Fop'
import KitchenAcc from '../screens/guest/accessories/KitchenAcc'
import Aventos from '../screens/guest/accessories/Aventos'
import AventosFiles from '../screens/guest/accessories/AventosFiles'
import Wood from '../screens/guest/subcategory';
import Saviola from '../screens/guest/subcategory/saviola'
import Alvic from '../screens/guest/subcategory/saviola/Alvic'
import Saviola1 from '../screens/guest/subcategory/saviola/Saviola'
import Luxe from '../screens/guest/subcategory/saviola/Luxe'
import AlvicCer from '../screens/guest/subcategory/saviola/AlvicCer'
import Media from '../screens/guest/subcategory/saviola/Media'
import History from '../screens/guest/subcategory/saviola/History'
import Certificates from '../screens/guest/subcategory/saviola/Certificates'
import Cer from '../screens/guest/subcategory/saviola/cer'
import Products from '../screens/guest/subcategory/saviola/Products';
import News from '../screens/guest/News';
import Contact from '../screens/guest/Contact';
import SideBar from '../components/general/SideBar';
import { NavigationRoutes } from '../constants';

var backCount = 0
export default class Route extends Component {

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('backPress', this.handleBackPress);
  }

  handleBackPress = () => {
    console.log('current scene--', Actions.currentScene)
    // || Actions.currentScene == 'Guest'  || Actions.currentScene == 'SignIn'
    if (Actions.currentScene == 'Welcome' || Actions.currentScene == 'Guest') {
      backCount += 1;
      if ((backCount < 2)) {
        ToastAndroid.show('Press again to Exit App!', ToastAndroid.SHORT);
        setTimeout(() => {
          backCount = 0;
        }, 2000);
        return true
      } else {
        BackHandler.exitApp();
        return true
      }
    }
  }

  render() {
    const scenes = Actions.create(
      <Scene key="root" navTransparent hideNavBar>
        <Scene key={NavigationRoutes.Welcome} component={Welcome} initial />
        <Scene key={NavigationRoutes.Intro} component={Intro} />
        <Scene key={NavigationRoutes.SignIn} component={SignIn} />
        <Scene key={NavigationRoutes.SignUp} component={SignUp} />
        <Scene key={NavigationRoutes.Forgot} component={Forgot} />
        <Drawer
          key="drawer"
          onExit={() => {
            console.log('Drawer closed');
          }}
          onEnter={() => {
            console.log('Drawer opened');
          }}
          drawerPosition={'left'}
          contentComponent={(props) => <SideBar {...props} />}
          drawerIcon={<Icon name="menu" type='Feather' style={{ color: Color.Grey656466, fontSize: 25 }} />}
          drawerWidth={250}
        >
         
          <Router key="guest" navigationBarStyle={{ backgroundColor: Color.White }} >
            <Scene key={NavigationRoutes.Guest} component={Guest} title={'HOME'} titleStyle={{ fontWeight: 'bold' }} initial />
            <Scene key={NavigationRoutes.IntroGuest} component={IntroGuest} titleStyle={{ fontWeight: 'bold' }} />
          </Router>
          <Router key="_signin" navigationBarStyle={{ backgroundColor: Color.White }}>
            <Scene key={'signin1'} component={SignIn} title='Sign-in' titleStyle={{fontWeight:'bold'}}  />
          </Router>
          <Router key="wood" navigationBarStyle={{ backgroundColor: Color.White }} >
            <Scene key={NavigationRoutes.Wood} component={Wood} titleStyle={{ fontWeight: 'bold' }} initial />
            <Scene key={NavigationRoutes.Saviola} component={Saviola} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.Products} component={Products} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.Media} component={Media} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.History} component={History} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.Certificates} component={Certificates} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.Cer} component={Cer} titleStyle={{ fontWeight: 'bold' }} hideNavBar />
            <Scene key={NavigationRoutes.Saviola1} component={Saviola1} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.Alvic} component={Alvic} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.Luxe} component={Luxe} titleStyle={{ fontWeight: 'bold' }} />
            <Scene key={NavigationRoutes.AlvicCer} component={AlvicCer} titleStyle={{ fontWeight: 'bold' }} />
          </Router>
          <Router key="accessories" navigationBarStyle={{ backgroundColor: Color.White }} >
             <Scene key={NavigationRoutes.Accessories} component={Accessories}  titleStyle={{fontWeight:'bold'}} initial /> 
             <Scene key={NavigationRoutes.Blum} component={Blum}  titleStyle={{fontWeight:'bold'}} /> 
             <Scene key={NavigationRoutes.Vibo} component={Vibo}  titleStyle={{fontWeight:'bold'}} /> 
             <Scene key={NavigationRoutes.Vauth_Sagel} component={Vauth_Sagel}  titleStyle={{fontWeight:'bold'}} /> 
             <Scene key={NavigationRoutes.System_Holz} component={System_Holz}  titleStyle={{fontWeight:'bold'}} /> 
             <Scene key={NavigationRoutes.Fop} component={Fop}  titleStyle={{fontWeight:'bold'}} /> 
             <Scene key={NavigationRoutes.KitchenAcc} component={KitchenAcc}  titleStyle={{fontWeight:'bold'}} /> 
             <Scene key={NavigationRoutes.Aventos} component={Aventos}  titleStyle={{fontWeight:'bold'}} /> 
             <Scene key={NavigationRoutes.AventosFiles} component={AventosFiles}  titleStyle={{fontWeight:'bold'}} /> 
          </Router>
          <Router key="news" navigationBarStyle={{ backgroundColor: Color.White }} >
            <Scene key={NavigationRoutes.News} component={News} titleStyle={{ fontWeight: 'bold' }} initial />
          </Router>
          <Router key="contact" navigationBarStyle={{ backgroundColor: Color.White }} >
            <Scene key={NavigationRoutes.Contact} component={Contact} titleStyle={{ fontWeight: 'bold' }} initial />
          </Router>
          <Router key="change" navigationBarStyle={{ backgroundColor: Color.White }} >
            <Scene key={NavigationRoutes.ChangePassword} component={ChangePassword} title='CHANGE PASSWORD' titleStyle={{ fontWeight: 'bold' }} initial />
          </Router>
        </Drawer>
      </Scene>
    )
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        enabled={Platform.OS === 'ios'}
        style={{ flex: 1 }}>
        <Router scenes={scenes} />
      </KeyboardAvoidingView>
    );
  }
}