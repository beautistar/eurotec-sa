import React, { useState } from 'react';
import {
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity, Keyboard
} from 'react-native';
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button, TextInput } from '../../components'
import { Actions } from 'react-native-router-flux';
import Api from '../../services/AppApi'
import AsyncStorage from '@react-native-community/async-storage';
import { strings,isRTL,isrtl } from '../../languages/I18n';
var CryptoJS = require("crypto-js");
const SignUp = () => {
    const [firstname, setFirstname] = useState()
    const [lastname, setLastname] = useState()
    const [username, setUsername] = useState()
    const [email, setEmail] = useState()
    const [mobileno, setMobileno] = useState()
    const [password, setPassword] = useState()
    const [confirmpass, setConfirmpass] = useState()
    const [error, setError] = useState()
    const [loading, setLoading] = useState(false)

    function signup() {
        Keyboard.dismiss()
        let emailVal = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let mobileVal = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        var encryptPassword = CryptoJS.AES.encrypt(password, 'secret').toString();
        if (firstname && lastname && username && email && mobileno && password && confirmpass) {
            if (emailVal.test(email) == false) {
                setError(strings('Please enter valid email address'))
            } else if (mobileVal.test(mobileno) == false) {
                setError(strings("Please Enter 10 digit Mobile Number"))
            } else if (password != confirmpass) {
                setError(strings("Password doesn't match"))
            } else {
                try{
                data = {
                    email: email,
                    phoneNumber: mobileno,
                    password: encryptPassword,
                    firstName: firstname,
                    lastName: lastname,
                    userName: username,
                    isAdmin: false
                }
                setLoading(true)
                setError('')
                Api.register(data).then((response) => {
                    console.log('register response----', response)
                    if (response.code == 200) {
                        AsyncStorage.setItem('login_data',JSON.stringify(response.payload))
                        AsyncStorage.setItem('token',JSON.stringify(response.payload.authToken))
                        setFirstname('')
                        setLastname('')
                        setUsername('')
                        setEmail('')
                        setMobileno('')
                        setPassword('')
                        setConfirmpass('')
                        setError('')
                        setLoading(false)
                        // Actions.guest()
                        Actions.reset('drawer')
                    }else{
                        setLoading(false)
                        setError(response.err)
                    }
                })
            }
          catch (error) {
                setError(error)
            }
        }
        } else {
            setError(strings('Please fill all fields'))
        }
    }
    return (
        <BaseScreen>
            <View style={styles.containner}>
                <View style={styles.subContainer}>
                    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'}
                        contentContainerStyle={{ paddingBottom: '15%' }}>
                        <View style={{ alignItems: 'center', marginBottom: Dimensions.moderateScale(25) }}>
                            <Text style={{ fontSize: Dimensions.FontSize.extraHuge, color: Color.orenge, fontWeight: "bold" }}>{strings('Sign Up')}</Text>
                        </View>
                        <View style={{ width: '100%', alignSelf: 'center' }}>
                            <View style={styles.signinView}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.usernameStyle}
                                    title={'First Name'}
                                    disabled={true} />
                                <TextInput
                                    textProps={styles.textinputStyle}
                                    keyboardType="default"
                                    maxLength={20}
                                    onChangeText={(text) => setFirstname(text)}
                                    value={firstname} />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.passwordStyle}
                                    title={'Last Name'}
                                    disabled={true} />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="default"
                                    maxLength={20}
                                    onChangeText={(text) => setLastname(text)}
                                    value={lastname} />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.passwordStyle}
                                    title={'User Name'}
                                    disabled={true} />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="default"
                                    maxLength={20}
                                    onChangeText={(text) => setUsername(text)}
                                    value={username} />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.passwordStyle}
                                    title={'Email Id'}
                                    disabled={true} />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="email-address"
                                    onChangeText={(text) => setEmail(text)}
                                    value={email} />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.passwordStyle}
                                    title={'Mobile No'}
                                    disabled={true} />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="number-pad"
                                    maxLength={10}
                                    onChangeText={(text) => setMobileno(text)}
                                    value={mobileno} />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.passwordStyle}
                                    title={'Password'}
                                    disabled={true} />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="default"
                                    maxLength={20}
                                    secureTextEntry={true}
                                    onChangeText={(text) => setPassword(text)}
                                    value={password} />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={[styles.buttonText, { textAlign: 'center' }]}
                                    containerStyle={styles.passwordStyle}
                                    title={'Confirm Password'}
                                    disabled={true} />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="default"
                                    maxLength={20}
                                    secureTextEntry={true}
                                    onChangeText={(text) => setConfirmpass(text)}
                                    value={confirmpass} />
                            </View>
                            <View style={{ marginTop: Dimensions.moderateScale(5) }}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.signinStyle}
                                    title={loading ? 'loading' : 'Sign Up'}
                                    onPress={() => signup()}
                                />
                            </View>
                            <Text style={styles.errorText}>{error ? error : null}</Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{backgroundColor:Color.White,width:"100%",position: 'absolute',bottom:0}}>
                <TouchableOpacity style={styles.bottomView} onPress={() => Actions.SignIn()}>
                    <Text style={{ fontSize: Dimensions.FontSize.medium }}>{strings('Account')}  </Text>
                    <Text style={{ fontSize: Dimensions.FontSize.medium, color: Color.orenge, fontWeight: "bold" }}> {strings('Sign in')}</Text>
                </TouchableOpacity>
                </View>
            </View>
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        justifyContent: 'center'
    },
    subContainer: {
        paddingHorizontal: Dimensions.moderateScale(25),
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    usernameStyle: {
        width: '40%',
        height: Dimensions.moderateScale(45),
    },
    buttonText: {
        color: Color.orenge,
        fontSize: Dimensions.FontSize.large,
    },
    textinputStyle: {
        width: '58%',
    },
    passwordStyle: {
        width: '40%',
        height: Dimensions.moderateScale(45),
    },
    signinStyle: {
        height: Dimensions.moderateScale(45),
    },
    bottomView: {
        flexDirection: 'row',
        alignItems: "center",
         bottom: Dimensions.moderateScale(12),
        alignSelf: 'center',
        padding: Dimensions.moderateScale(10)
    },
    errorText: {
        marginTop: Dimensions.Spacing.medium,
        color: Color.Red,
        alignSelf: "center"
    }
});

export default SignUp;
