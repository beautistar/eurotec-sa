import React from 'react';
import {
  StyleSheet,
  Text,
  View, TouchableOpacity,
  Image,Linking
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo } from '../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage';
const RenderImage = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} activeOpacity={0.9} style={{borderWidth:2,
      borderColor: Color.GreyMedium}}>
      {/* <FastImage source={{uri:props.source,priority:FastImage.priority.high}} style={styles.image}  resizeMode={FastImage.resizeMode.cover} /> */}
      <Image source={props.source} resizeMode='cover' style={styles.image} />
    </TouchableOpacity>
  )
}
const Welcome = () => {
  function onClick (lang) {
    AsyncStorage.setItem('Language', lang)
    Actions.Intro()
  }
  return (
    <BaseScreen>
      <View style={styles.containner}>
        <View style={styles.subContainer}>
          <Logo />
          <View>
          <RenderImage source={Images.Images.English} onPress={()=>onClick('English')}/>
            <RenderImage source={Images.Images.ImageLang} onPress={()=>onClick('Arabic')}/>
          </View>
      </View>
      <View style={styles.bottomView}>
          <TouchableOpacity style={{padding:5}} onPress={()=>Linking.openURL('http://www.eurotec.sa.com/')}>
          <Text style={{ fontSize: Dimensions.FontSize.medium,color:Color.Gray656466 }}>www.eurotec.sa.com</Text>
          </TouchableOpacity>
        </View>
      </View>
    </BaseScreen>
  );
};

const styles = StyleSheet.create({
  containner: {
    flex: 1,
    justifyContent: 'center',
  },
  subContainer: {
    height: '60%',
    justifyContent: 'space-between'
  },
  imageContainer: {
    width: '70%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  image: {
    width: '100%',
    height: Dimensions.moderateScale(115),
  },
  bottomView: {
    alignSelf: "center",
    position: 'absolute',
    bottom: Dimensions.moderateScale(12)
  }
});

export default Welcome;
