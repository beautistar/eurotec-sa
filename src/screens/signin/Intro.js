import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    View, TouchableOpacity, ActivityIndicator,
    Image,Linking
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button } from '../../components'
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { strings } from '../../languages/I18n';
const Intro = () => {
    var news = strings('News & Events')
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            console.log('token--', JSON.parse(response))
            if (JSON.parse(response) != null) {
                console.log('guest')
                Actions.reset('drawer')
            } else {
                setLoading(false)
            }
        })
    }, [])

    if (loading)
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
        )
    else
        return (
            <BaseScreen>
                <View style={styles.containner}>
                    <View style={styles.subContainer}>
                        <Logo />
                        <View style={{ alignItems: 'center' }}>
                            <Button
                                containerStyle={styles.signinStyle}
                                title={'Login'}
                                onPress={() => Actions.SignIn()} />
                            <Button
                                containerStyle={styles.guestStyle}
                                title={'Guest'}
                                // Actions.guest()
                                onPress={() => Actions.drawer()} />
                        </View>
                    </View>
                    <View style={styles.bottomView}>
                        <TouchableOpacity style={{ padding: 5 }} onPress={() => Linking.openURL('http://www.eurotec.sa.com/')}>
                            <Text style={{ fontSize: Dimensions.FontSize.medium, color: Color.Gray656466 }}>www.eurotec.sa.com</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </BaseScreen>
        );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        justifyContent: 'center'
    },
    subContainer: {
        marginTop:Dimensions.moderateScale(-30),
        height: '50%',
        justifyContent: 'space-between'
    },
    signinStyle: {
        width: '75%',
    },
    guestStyle: {
        width: '75%',
        marginTop: Dimensions.moderateScale(10)
    },
    bottomView: {
        alignSelf: "center",
        position: 'absolute',
        bottom: Dimensions.moderateScale(12)
    }
});

export default Intro;
