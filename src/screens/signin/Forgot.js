import React, { useState } from 'react';
import {
    StyleSheet,
    Text, ScrollView, TextInput as TI,
    View, TouchableOpacity, Keyboard
} from 'react-native';
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button, TextInput } from '../../components'
import { Actions } from 'react-native-router-flux';
import Api from '../../services/AppApi'
import { strings } from '../../languages/I18n';
const Forgot = () => {
    const [email, setEmail] = useState()
    const [error, setError] = useState()
    const [loading, setLoading] = useState(false)

    function submit() {
        Keyboard.dismiss()
        let emailVal = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        if (email) {
            if (emailVal.test(email) == false) {
                setError(strings('Please enter valid email address'))
            } else {
                data = {
                    email: email
                }
                setLoading(true)
                setError('')
                Api.forgot(data).then((response) => {
                    console.log('forgot response----', response)
                    if (response.code == 200) {
                        setEmail('')
                        setError('')
                        setLoading(false)
                        Actions.reset('SignIn')
                    } else {
                        setLoading(false)
                        setError(response.err)
                    }
                })
            }
        } else {
            setError(strings('Please fill Email address field'))
        }
    }
    return (
        <BaseScreen
        >
            <View style={styles.containner}>
                <View style={styles.subContainer}>
                    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'}
                        contentContainerStyle={{ paddingBottom: '15%' }}>
                        <View style={{ alignItems: 'center', marginBottom: Dimensions.moderateScale(25) }}>
                            <Text style={{ fontSize: Dimensions.FontSize.extraHuge, color: Color.orenge, fontWeight: "bold" }}>{strings('Forgot Password')}</Text>
                        </View>
                        <View style={{ width: '100%', alignSelf: 'center' }}>
                            <View style={styles.signinView}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.usernameStyle}
                                    title={'Email Id'}
                                    disabled={true} />
                                <TextInput
                                    textProps={styles.textinputStyle}
                                    keyboardType="email-address"
                                    onChangeText={(text) => setEmail(text)}
                                    value={email} />
                            </View>
                            <View style={{ marginTop: Dimensions.moderateScale(5) }}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.signinStyle}
                                    title={loading ? 'loading' : 'Submit'}
                                    onPress={() => submit()}
                                />
                            </View>
                            <Text style={styles.errorText}>{error ? error : null}</Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{ backgroundColor: Color.White, width: "100%", position: 'absolute', bottom: 0 }}>
                    <TouchableOpacity style={styles.bottomView} onPress={() => Actions.SignIn()}>
                        <Text style={{ fontSize: Dimensions.FontSize.medium }}>{strings('Remembered your Password')}  </Text>
                        <Text style={{ fontSize: Dimensions.FontSize.medium, color: Color.orenge, fontWeight: "bold" }}> {strings('Sign in')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        justifyContent: 'center'
    },
    subContainer: {
        paddingHorizontal: Dimensions.moderateScale(25),
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    usernameStyle: {
        width: '40%',
        height: Dimensions.moderateScale(45),
    },
    buttonText: {
        color: Color.orenge,
        fontSize: Dimensions.FontSize.large,
    },
    textinputStyle: {
        width: '58%',
    },
    signinStyle: {
        height: Dimensions.moderateScale(45),
    },
    bottomView: {
        flexDirection: 'row',
        alignItems: "center",
        bottom: Dimensions.moderateScale(12),
        alignSelf: 'center',
        padding: Dimensions.moderateScale(10)
    },
    errorText: {
        marginTop: Dimensions.Spacing.medium,
        color: Color.Red,
        alignSelf: "center"
    }
});

export default Forgot;
