import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity, Keyboard
} from 'react-native';
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button, TextInput } from '../../components'
import { Actions } from 'react-native-router-flux';
import Api from '../../services/AppApi'
import AsyncStorage from '@react-native-community/async-storage';
import { strings } from '../../languages/I18n';
import { Toast, Root } from 'native-base';
var CryptoJS = require("crypto-js");
const SignUp = () => {
    const [token, settoken] = useState()
    const [password, setPassword] = useState()
    const [confirmpass, setConfirmpass] = useState()
    const [error, setError] = useState()
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            console.log('token--', JSON.parse(response))
            settoken(JSON.parse(response))
        })
    }, [])
    function submit() {
        Keyboard.dismiss()
        var encryptNewPassword = CryptoJS.AES.encrypt(password, 'secret').toString();
        var encryptConfirmPassword = CryptoJS.AES.encrypt(confirmpass, 'secret').toString();
        if (password && confirmpass) {
            if (password != confirmpass) {
                setError("Password doesn't match.")
            } else {
                data = {
                    newPassword: encryptNewPassword,
                    confirmPassword: encryptConfirmPassword,
                }
                setLoading(true)
                setError('')
                Api.changepassword(data, token).then((response) => {
                    console.log('change password response----', response)
                    if (response.code == 200) {
                        setPassword('')
                        setConfirmpass('')
                        setError('')
                        setLoading(false)
                        Toast.show({
                            text: "Password changed Successfully!",
                            duration: 2000,
                            style: {
                                backgroundColor: Color.orenge
                               }
                          })
                        // Actions.reset('drawer')
                    } else {
                        setLoading(false)
                        setError(response.err)
                    }
                })
            }
        } else {
            setError(strings('Please fill all fields'))
        }
    }
    return (
        <Root>
        <BaseScreen>
            <View style={styles.containner}>
                <View style={styles.subContainer}>
                    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'}
                        contentContainerStyle={{ paddingBottom: '15%' }}>
                        <View style={{ width: '100%', alignSelf: 'center' }}>
                            <View style={styles.signinView}>
                                <Button
                                    textStyle={styles.buttonText,{textAlign:'center'}}
                                    containerStyle={styles.usernameStyle}
                                    title={'New Password'}
                                    disabled={true} />
                                <TextInput
                                    textProps={styles.textinputStyle}
                                    keyboardType="default"
                                    maxLength={20}
                                    secureTextEntry={true}
                                    onChangeText={(text) => setPassword(text)}
                                    value={password} />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={[styles.buttonText, { textAlign: 'center' }]}
                                    containerStyle={styles.passwordStyle}
                                    title={'Confirm Password'}
                                    disabled={true} />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="default"
                                    maxLength={20}
                                    secureTextEntry={true}
                                    onChangeText={(text) => setConfirmpass(text)}
                                    value={confirmpass} />
                            </View>
                            <View style={{ marginTop: Dimensions.moderateScale(5) }}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.signinStyle}
                                    title={loading ? 'loading' : 'Submit'}
                                    onPress={() => submit()}
                                />
                            </View>
                            <Text style={styles.errorText}>{error ? error : null}</Text>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </BaseScreen>
        </Root>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        justifyContent: 'center'
    },
    subContainer: {
        paddingHorizontal: Dimensions.moderateScale(25),
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    usernameStyle: {
        width: '40%',
        height: Dimensions.moderateScale(45),
    },
    buttonText: {
        color: Color.orenge,
        fontSize: Dimensions.FontSize.large,
    },
    textinputStyle: {
        width: '58%',
    },
    passwordStyle: {
        width: '40%',
        height: Dimensions.moderateScale(45),
    },
    signinStyle: {
        height: Dimensions.moderateScale(45),
    },
    bottomView: {
        flexDirection: 'row',
        alignItems: "center",
        bottom: Dimensions.moderateScale(12),
        alignSelf: 'center',
        padding: Dimensions.moderateScale(10)
    },
    errorText: {
        marginTop: Dimensions.Spacing.medium,
        color: Color.Red,
        alignSelf: "center"
    }
});

export default SignUp;
