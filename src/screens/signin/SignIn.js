import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View, TouchableOpacity, ScrollView,
    Image, Keyboard
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button, TextInput } from '../../components'
import { Actions } from 'react-native-router-flux';
import Api from '../../services/AppApi'
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import { strings } from '../../languages/I18n';
var CryptoJS = require("crypto-js");

const SignIn = () => {
    const [username, setUsername] = useState()
    const [password, setPassword] = useState()
    const [error, setError] = useState()
    const [loading, setLoading] = useState(false)

    function signin() {
        Keyboard.dismiss()
        if (username && password) {
            var encryptPassword = CryptoJS.AES.encrypt(password, 'secret').toString();
          var data = {
                username: username,
                password: encryptPassword
            }
                setLoading(true)
                setError('')
                Api.login(data).then((response) => {
                    console.log('login response---', response)
                    if (response.code == 200) {
                        AsyncStorage.setItem('login_data', JSON.stringify(response.payload))
                        AsyncStorage.setItem('token', JSON.stringify(response.payload.authToken))
                        setUsername('')
                        setPassword('')
                        setError('')
                        setLoading(false)
                        // Actions.guest()
                        Actions.reset('drawer')
                    } else {
                        setLoading(false)
                        setError(response.err)
                    }
                })
        } else {
            setError(strings('Please fill all fields'))
        }
    }
    return (
        <BaseScreen>
            <View style={styles.containner}>
                <View style={styles.subContainer}>
                    <ScrollView contentContainerStyle={{ paddingBottom: 20 }} showsVerticalScrollIndicator={false}>
                        <View style={styles.lockView}>
                            <FastImage source={{uri:Images.Images.Lock,priority:FastImage.priority.high}} style={styles.imageView} />
                        </View>
                        <View style={{ width: '85%', alignSelf: 'center' }}>
                            <View style={styles.signinView}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.usernameStyle}
                                    title={'User Name'}
                                    disabled={true}
                                />
                                <TextInput
                                    textProps={styles.textinputStyle}
                                    keyboardType="default"
                                    maxLength={20}
                                    onChangeText={(text) => setUsername(text)}
                                    value={username}
                                />
                            </View>
                            <View style={[styles.signinView, { marginTop: Dimensions.moderateScale(5) }]}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.passwordStyle}
                                    title={'Password'}
                                    disabled={true}
                                />
                                <TextInput
                                    textProps={{ width: '58%', }}
                                    keyboardType="default"
                                    maxLength={20}
                                    secureTextEntry={true}
                                    onChangeText={(text) => setPassword(text)}
                                    value={password}
                                />
                            </View>
                            <View style={{ marginTop: Dimensions.moderateScale(5) }}>
                                <Button
                                    textStyle={styles.buttonText}
                                    containerStyle={styles.signinStyle}
                                    title={loading ? 'loading' : 'Sign-In'}
                                    onPress={() => signin()}
                                />
                            </View>
                            <Text style={styles.errorText}>{error ? error : null}</Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{ backgroundColor: Color.White, width: "100%", position: 'absolute', bottom: 0 }}>
                    <View style={styles.bottomView} >
                        <TouchableOpacity onPress={() => Actions.SignUp()}>
                            <Text style={{ fontSize: Dimensions.FontSize.medium, color: Color.Grey656466, fontWeight: "bold" }}>{strings('Create Account')} - </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Actions.Forgot()}>
                            <Text style={{ fontSize: Dimensions.FontSize.medium, color: Color.Grey656466, fontWeight: "bold" }}> {strings('Forgot Password')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        justifyContent: 'center'
    },
    subContainer: {
        paddingHorizontal: Dimensions.moderateScale(25),
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    buttonText: {
        fontSize: Dimensions.FontSize.large,
    },
    usernameStyle: {
        width: '40%',
        height: Dimensions.moderateScale(45),
    },
    textinputStyle: {
        width: '58%',
    },
    passwordStyle: {
        width: '40%',
        height: Dimensions.moderateScale(45),
    },
    signinStyle: {
        height: Dimensions.moderateScale(45),
    },
    lockView: {
        width: Dimensions.moderateScale(100),
        height: Dimensions.moderateScale(100),
        borderRadius: Dimensions.moderateScale(50),
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginBottom: Dimensions.moderateScale(20)
    },
    imageView: {
        width: Dimensions.moderateScale(100),
        height: Dimensions.moderateScale(100),
        borderRadius: Dimensions.moderateScale(50),
    },
    bottomView: {
        flexDirection: 'row',
        alignItems: 'center',
        bottom: Dimensions.moderateScale(12),
        alignSelf: 'center',
        padding: Dimensions.moderateScale(5)
    },
    errorText: {
        marginTop: Dimensions.Spacing.medium,
        color: Color.Red,
        alignSelf: "center"
    }
});

export default SignIn;
