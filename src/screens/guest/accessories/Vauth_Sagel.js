import React, { useState, useEffect } from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList, ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button, ImageGallery } from '../../../components'
import { Actions } from 'react-native-router-flux';
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const VauthSagel = (props) => {
    // console.log('VauthSagel props', props)
    const [vauthSagel, setvauthSagel] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getproducts(props.id).then((response) => {
            console.log('vauthSagel respone---', response)
            if (response.code == 200) {
                setvauthSagel(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    return (
        <BaseScreen technical={true} tecOnPress={() => { }}>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                vauthSagel && vauthSagel.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                       
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <ImageGallery data={vauthSagel} />
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
});

export default VauthSagel;
