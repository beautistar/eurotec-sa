import React,{useState,useEffect} from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button } from '../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const RenderView = (props) => {
    return (
        <View style={{ height: props.dheight ? props.dheight / 5 : '100%', flexDirection: 'row' }}>
        <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}
            style={[props.imageview, { width: "100%", }]}>
            <FastImage source={{ uri: props.image, priority: FastImage.priority.high }} resizeMode={FastImage.resizeMode.stretch} style={{ height: '100%', width: '100%', }}>
            </FastImage>
        </TouchableOpacity>
        </View>
    )
}
const Accessories = (props) => {
    console.log('acc props---',props)
    const [accessories, setAccessories] = useState()
    const [dheight, setdHeight] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
            Api.subcategories(props.id).then((response) => {
                console.log('sub cat respone---', response)
                if (response.code == 200) {
                    setAccessories(response.payload)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
    }, [])
    if (loading)
    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator color={Color.orenge} size="large" />
        </View>
    )
else
    return (
        <BaseScreen>
               { accessories && accessories.length == 0 ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings('Accessories not found')}</Text>
                </View>
                :
            <View onLayout={(e) => setdHeight(e.nativeEvent.layout.height)} style={{ flex: 1, }}>
                <ScrollView showsVerticalScrollIndicator={false}>

                { accessories.map((item, index) =>
                <RenderView
                    onPress={() => {item.name == 'blum' ? Actions.Blum({ id: item._id, title: item.name.toUpperCase() }) 
                                    : item.name == 'vibo' ? Actions.Vibo({ id: item._id, title: item.name.toUpperCase()}) 
                                    : item.name == 'vauth sagel' || item.name == 'vauth-sagel' ? Actions.Vauth_Sagel({ id: item._id, title: item.name.toUpperCase() }) 
                                    : item.name == 'system holz' ? Actions.System_Holz({ id: item._id, title: item.name.toUpperCase() }) 
                                    : Actions.Fop({ id: item._id, title:item.name.toUpperCase() })}}
                    // Images.Images.http + item.image.split("/")[3]
                    image={item.image}
                    dheight={dheight}
                />
                )} 
                </ScrollView>
            </View>
}
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        height: (dm.get('screen').height - 55) / 5.5,
        width: '100%',
    },
});

export default Accessories;
