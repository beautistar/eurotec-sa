import React, { useEffect, useState } from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image, ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button } from '../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const RenderView = (props) => {
    return (
        <View style={{ height: props.dheight ? props.dheight / 2 : '100%' }}>
            <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}
                style={styles.viewContainer}>
                <FastImage source={{ uri: props.image, priority: FastImage.priority.high }} resizeMode={FastImage.resizeMode.stretch} style={styles.image}>
                </FastImage>
            </TouchableOpacity>
        </View>
    )
}
const Vibo = (props) => {
    console.log('vibo props', props)
    const [vibo, setVibo] = useState()
    const [dheight, setdHeight] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getproducts(props.id).then((response) => {
            console.log('vibo respone---', response)
            if (response.code == 200) {
                setVibo(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    if (loading)
    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator color={Color.orenge} size="large" />
        </View>
    )
else
    return (
        <BaseScreen technical={true} tecOnPress={() => { }}>
               { vibo && vibo.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <View onLayout={(e) => setdHeight(e.nativeEvent.layout.height)} style={{ flex: 1, }}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {vibo.map((item, index) =>
                                <RenderView
                                    onPress={() => { Actions.KitchenAcc({id: item._id,title: item.name.toUpperCase() ,isSubProduct : item.isSubProduct }) }}
                                    image={item.image}
                                    dheight={dheight}
                                />
                            )}
                        </ScrollView>
                    </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        width: '100%',
    },
    image:{
        height: '100%',
         width: '100%', 
    }
});

export default Vibo;
