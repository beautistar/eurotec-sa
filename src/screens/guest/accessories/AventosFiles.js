import React,{useState,useEffect} from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList,ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button, File } from '../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import { Icon } from 'native-base';
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const AventosFiles = (props) => {
    console.log('files props------', props)
    const [files, setFiles] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        if (props.isSubProduct && props.isSubProduct == true) {
            Api.getfilesbysubproduct(props.id).then((response) => {
                console.log('subproduct File respones---', response)
                if (response.code == 200) {
                    setFiles(response.payload.media)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        } else {
            Api.getfilesbyproduct(props.id).then((response) => {
                console.log('product file respones---', response)
                if (response.code == 200) {
                    setFiles(response.payload.media)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        }

    }, [])
    return (
        <BaseScreen technical={true} tecOnPress={() => { }}>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                files && files.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <View style={{ flex: 1, paddingVertical: Dimensions.moderateScale(10), paddingHorizontal: Dimensions.moderateScale(20) }}>
                        <File data={files} />
                    </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
});

export default AventosFiles;
