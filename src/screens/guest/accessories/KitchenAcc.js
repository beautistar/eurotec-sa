import React, { useState, useEffect } from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList, ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button } from '../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const RenderView = (props) => {
    let { data } = props;
    return (
        <TouchableOpacity style={[styles.viewContainer,{alignSelf:props.index != props.kitchenAcc.length - 1 ? 'flex-end' : null,}]}
            activeOpacity={0.8} onPress={() => {Actions.AventosFiles({ title: data.name.toUpperCase() , id:data._id,isSubProduct : props.isSubProduct }) }}>
            <FastImage source={{ uri: data.image, priority: FastImage.priority.high }} resizeMode={FastImage.resizeMode.stretch} style={styles.image} />
        </TouchableOpacity>
    )
}
const KitchenAcc = (props) => {
    console.log('props-------', props)
    const [kitchenAcc, setkitchenAcc] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getsubproducts(props.id).then((response) => {
            console.log('kitchenAcc respone---', response)
            if (response.code == 200) {
                setkitchenAcc(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    return (
        <BaseScreen>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                kitchenAcc && kitchenAcc.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <View style={{ flex: 1, }}>
                        <FlatList
                            contentContainerStyle={{}}
                            showsVerticalScrollIndicator={false}
                            data={kitchenAcc}
                            extraData={kitchenAcc}
                            renderItem={({ item, index }) => <RenderView data={item} isSubProduct = {props.isSubProduct} index={index} kitchenAcc={kitchenAcc}/>}
                            // keyExtractor={item => item.id}
                            numColumns={2}
                        />
                    </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        height: (dm.get('window').height - 56) / 2,
        width: '50%',
        elevation: 2.5,
        margin: 0.25,
    },
    image: {
        width: '100%',
        height: '100%'
    },
});

export default KitchenAcc;
