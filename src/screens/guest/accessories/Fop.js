import React, { useEffect, useState } from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList, ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button } from '../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const RenderView = (props) => {
    let { data } = props;
    return (
        <TouchableOpacity style={[styles.viewContainer,{ height: props.dheight ? props.dheight / 2 : '100%'}]}
            activeOpacity={0.8} onPress={() => {Actions.AventosFiles({ title: data.name.toUpperCase(),id:data._id,isSubProduct:data.isSubProduct }) }}>
            <FastImage source={{ uri: data.image, priority: FastImage.priority.high }} resizeMode={FastImage.resizeMode.stretch} style={styles.image} />
        </TouchableOpacity>
    )
}
const Fop = (props) => {
    console.log('fop props', props)
    const [fop, setFop] = useState()
    const [dheight, setdHeight] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getproducts(props.id).then((response) => {
            console.log('fop respone---', response)
            if (response.code == 200) {
                setFop(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    if (loading)
    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator color={Color.orenge} size="large" />
        </View>
    )
else
    return (
        <BaseScreen technical={true} tecOnPress={() => { }}>
               { fop && fop.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <View  onLayout={(e) => setdHeight(e.nativeEvent.layout.height)} style={{ flex: 1, }}>
                        <FlatList
                            contentContainerStyle={{}}
                            showsVerticalScrollIndicator={false}
                            data={fop}
                            extraData={fop}
                            renderItem={({ item, index }) => <RenderView data={item} dheight={dheight}/>}
                            // keyExtractor={item => item.id}
                            numColumns={2}
                        />
                    </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        flex: 1,
        flexDirection: 'row',
        // height: (dm.get('window').height - ((dm.get('window').height - 56) / 5)) / 2.2
    },
    image: {
        width: '100%',
    },
});

export default Fop;
