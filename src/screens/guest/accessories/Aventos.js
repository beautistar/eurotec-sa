import React, { useState, useEffect } from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList, ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button } from '../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const RenderView = (props) => {
    let { data } = props;
    console.log('props-------', props)
    return (
        <TouchableOpacity
            style={props.title == 'Aventos Systems' || props.title == 'aventos systems' || props.title == ('aventos systems').toUpperCase() ?
                [styles.viewContainer, { height: props.dheight ? props.dheight / 4 : '100%' }]
                :
                [styles.viewContainer1, { height: props.dheight ? props.dheight / 4 : '100%' }]}
            activeOpacity={0.8} onPress={() => { Actions.AventosFiles({ title: data.name.toUpperCase(), id: data._id, isSubProduct: props.isSubProduct }) }}>
            <FastImage source={{ uri: data.image, priority: FastImage.priority.high }} resizeMode={FastImage.resizeMode.stretch} style={styles.image} />
        </TouchableOpacity>
    )
}
const Aventos = (props) => {
    console.log('aventos props----', props)
    const [aventos, setAventos] = useState()
    const [dheight, setdHeight] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getsubproducts(props.id).then((response) => {
            console.log('Aventos respone---', response)
            if (response.code == 200) {
                setAventos(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    if (loading)
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
        )
    else
        return (
            <BaseScreen >
                {aventos && aventos.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <View onLayout={(e) => setdHeight(e.nativeEvent.layout.height)} style={{ flex: 1, }}>
                        <FlatList
                            contentContainerStyle={{}}
                            showsVerticalScrollIndicator={false}
                            data={aventos}
                            extraData={aventos}
                            renderItem={({ item, index }) => <RenderView data={item} title={props.title} isSubProduct={props.isSubProduct} dheight={dheight} />}
                            keyExtractor={item => item.id}
                            numColumns={2}
                        />
                    </View>
                }
            </BaseScreen>
        );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        flex: 1,
        flexDirection: 'row',
        // height: (dm.get('window').height - 56) / 4,
        // width:'50%'
    },
    viewContainer1: {
        // flex: 1, 
        flexDirection: 'row',
        // height: (dm.get('window').height - 56) / 4,
        width: '50%'
    },
    image: {
        width: '100%',
        height: '100%'
    },
});

export default Aventos;
