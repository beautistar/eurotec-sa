import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View, TouchableOpacity, ImageBackground, ActivityIndicator,
    Image, FlatList,
    Dimensions as dm,
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button } from '../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../../services/AppApi'

const RenderView = (props) => {
    var { data } = props;
    console.log('props--', data)
    return (
        <View style={styles.cardView}>
            <FastImage source={{ uri: data.image , priority: FastImage.priority.high }} style={styles.image} resizeMode={FastImage.resizeMode.stretch}>
            </FastImage>
        </View>
    )
}
const Contact = () => {
    const [token, settoken] = useState()
    const [contact, setContact] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            console.log('contact token--', JSON.parse(response))
            settoken(JSON.parse(response))
            Api.contact(JSON.parse(response)).then((response) => {
                console.log('contact response---', response)
                if (response.code == 200) {
                    setContact(response.payload)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        })
    }, [])
    return (
        <BaseScreen>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                contact && contact.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>No Data Found..</Text>
                    </View>
                    :
                    <FlatList showsVerticalScrollIndicator={false}
                        style={{ flex: 1,}}
                        data={contact}
                        renderItem={({ item, index }) => <RenderView data={item} />}
                        keyExtractor={item => item.id}
                    />
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: '100%',
        elevation: 2
    },
    cardView: {
        height: (dm.get('window').height-56) / 3,
        paddingVertical: Dimensions.moderateScale(0.5),
        elevation: 5
    },
});

export default Contact;
