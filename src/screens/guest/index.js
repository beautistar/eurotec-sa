import React, { useEffect, useState } from 'react';
import {
    StyleSheet, ActivityIndicator,
    Text, ScrollView,
    View, TouchableOpacity,
    ImageBackground, Alert,
    Dimensions as dm
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button } from '../../components'
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import Api from '../../services/AppApi'
import { strings } from '../../languages/I18n';

const RenderView = (props) => {
    return (
        <View style={{ height: (dm.get('window').height-56) / 5 , }}>
            <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}
                style={[styles.viewContainer, props.viewstyle, { backgroundColor: props.color, }]}>
                <FastImage source={{ uri: props.image, priority: FastImage.priority.high }} resizeMode={props.resizeMode} style={{ height: '100%', width: '100%', }}>
                    <View style={props.textProps}>
                        <Text style={[styles.textStyle, props.textColor]}>{strings(props.text)}</Text>
                        {props.product ? <Text style={[styles.prodTextStyle, props.prodText]}>{strings(props.product)}</Text> : null}
                        {props.questionmark ? <Text style={props.question}>{strings(props.questionmark)}</Text> : null}
                    </View>
                   
                </FastImage>
            </TouchableOpacity>
        </View>
    )
}
const RenderApiView = (props) => {
    return (
        <View style={{ height:(dm.get('window').height-56) / 5, }}>
            <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}
                style={[styles.viewContainer, props.viewstyle, { backgroundColor: props.color, }]}>
                <FastImage source={{ uri: props.image, priority: FastImage.priority.high }} resizeMode={props.resizeMode} style={{ height: '100%', width: '100%', }}>
                    <View style={[props.textProps, { maxWidth: 300 }]}>
                        <Text numberOfLines={2} style={[styles.textStyle, props.textColor]}>
                            {props.text.toUpperCase()}
                        </Text>
                        {props.product ? <Text style={[styles.prodTextStyle, props.prodText]}>{strings(props.product)}</Text> : null}
                    </View>
                </FastImage>
            </TouchableOpacity>
        </View>
    )
}
const Guest = () => {
    const [categories, setCategories] = useState()
    const [loading, setLoading] = useState()
    const [error, setError] = useState()
    const [code, setCode] = useState()
    var title = strings('Contact')
    var news = strings('News & Events')
    var whoweare = strings('Who we are?')
    function getdata() {
        setLoading(true)
        AsyncStorage.getItem("token").then((response) => {
            console.log('token--', JSON.parse(response))
            Api.categories(JSON.parse(response)).then((response) => {
                console.log('category respone---', response)
                if (response.code == 200) {
                    setCategories(response.payload)
                    setLoading(false)
                }
                else {
                    setError(response.message)
                    setLoading(false)
                }
            })
        })
    }
    useEffect(() => {
        AsyncStorage.getItem("login_data").then((response) => {
            console.log('async data--', JSON.parse(response))
        })
        getdata()
    }, [])

    if (loading)
    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator color={Color.orenge} size="large" />
        </View>
    )
else
    return (
        <BaseScreen>
            {/* {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                : */}
                { error ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{error}</Text>
                        <TouchableOpacity style={{ borderWidth: 1, padding: 5, marginTop: 10 }} onPress={() => { getdata() }}>
                            <Text>Try again</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{ flex: 1,backgrounColor:'red' }}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <RenderView text='Who'
                                onPress={() => Actions.IntroGuest({ title: whoweare })}
                                product={'we are'}
                                prodText={{ color: Color.White, paddingLeft: 7, paddingBottom: 5 }}
                                questionmark = {'??'}
                                question={{color:Color.White,fontSize:Dimensions.moderateScale(30),top:Dimensions.moderateScale(28),fontWeight: 'bold',right:Dimensions.moderateScale(15)}}
                                textProps={{ right: 25, position: 'absolute', paddingTop: Dimensions.moderateScale(7), flexDirection: 'row', alignItems: 'flex-end', }}
                                image={Images.Images.WhoWeAre} 
                              />
                            {categories ? categories.map((item, index) =>
                                <RenderApiView text={item.name}
                                    onPress={() => { item.uitype == 'ACC' ? Actions.reset('Accessories', { id: item._id, title: item.name.toUpperCase() }) : Actions.reset('Wood', { id: item._id, title: item.name.toUpperCase() }) }}
                                    product={'Products'}
                                    textColor={{ color: index == 1 ? Color.White : Color.orenge }}
                                    resizeMode={FastImage.resizeMode.cover}
                                    prodText={{ position: 'absolute', top: 5, left: -66, zIndex: 1, color: index == 1 ? Color.Brown432620 : Color.Black }}
                                    textProps={{ bottom: 8, right: index == 1 ? 10 : 40, position: 'absolute', paddingTop: Dimensions.moderateScale(10), }}
                                    image={Images.Images.http + item.image.split("/")[3]}
                                />
                            ) : null}
                            <RenderView text='NEWS EVENTS' onPress={() => Actions.reset('News', { title: news.toUpperCase() })}
                                product="&"
                                textColor={{ color: Color.Blue619ad9 }}
                                prodText={{ fontSize: Dimensions.moderateScale(40), color: Color.Brown8c8084, position: 'absolute', bottom: 20, left: 50, zIndex: 1 }}
                                textProps={{ bottom: 8, position: 'absolute', paddingLeft: Dimensions.moderateScale(10), width: Dimensions.moderateScale(150), }}
                                image={Images.Images.News} 
                              />
                            <RenderView
                                resizeMode={FastImage.resizeMode.stretch}
                                textProps={{ bottom: 8, position: 'absolute', paddingLeft: Dimensions.moderateScale(10) }}
                                text='CALL' onPress={() => Actions.reset('Contact', { title: title.toUpperCase() })} image={Images.Images.Contact} 
                                />
                        </ScrollView>
                    </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        width: '100%',
    },
    textStyle: {
        fontSize: Dimensions.moderateScale(35),
        color: Color.White,
        fontWeight: 'bold',
    },
    prodTextStyle: {
        fontSize: Dimensions.moderateScale(20),
        color: Color.Black,
        fontWeight: 'bold'
    }
});

export default Guest;
