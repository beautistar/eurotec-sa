import React, { useEffect, useState } from 'react';
import {
    StyleSheet, ActivityIndicator, FlatList,
    Text, ScrollView, Modal, TouchableWithoutFeedback,
    View, TouchableOpacity,Linking,
    ImageBackground,
    Dimensions as Dm
} from 'react-native';
import Images from '../../../../assets'
import { Color, Dimensions } from '../../../../helper'
import { BaseScreen, Logo, Button } from '../../../../components'
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../../../../services/AppApi'
import FastImage from 'react-native-fast-image'
import { WebView } from 'react-native-webview';
import { strings } from '../../../../languages/I18n';
const DATA = [
    {
        name: 'Blum',
        image: Images.Images.Blum
    },
    {
        name: 'Vibo',
        image: Images.Images.AlvicBG
    },
    {
        name: 'System Holz',
        image: Images.Images.Blum
    },
    {
        name: 'Car',
        image: Images.Images.AlvicBG
    },
    {
        name: 'Opes Save',
        image: Images.Images.Blum
    }
]
const RenderView = (props) => {
    let { data } = props;
    return (
        <TouchableOpacity style={{ flex: 1, paddingHorizontal: 2.5, paddingVertical: 2.5, flexDirection: 'row', }}
            activeOpacity={0.8} onPress={() => { props.setvisible(true, data) }}>
            {/* Images.Images.http + data.split("/")[3] */}
            <FastImage source={{ uri: Images.Images.http + data.split("/")[3], priority: FastImage.priority.high }} style={styles.image} />
        </TouchableOpacity>
    )
}

const RenderVideoView = (props) => {
    console.log('data---', props)
    let { data,index } = props;
    const [visible, setvisible] = useState(false)
    function download() {
        setvisible(true)
        setTimeout(() => {
            setvisible(false)
        }, 2000);
    }
    
    return (
        <View style={{  paddingHorizontal: 2.5, paddingVertical: 5,}}>
            <TouchableOpacity style={styles.viewContainer}
            // download()
                onPress={() => Linking.openURL(Images.Images.http + data.split("/")[3])}>
                <Text style={styles.textStyle}>{strings('Video ') + (index + 1)}</Text>
            </TouchableOpacity>
            {visible ?
                <WebView source={{ uri: Images.Images.http + data.split("/")[3] }} />
                : null}
        </View>
    )
}

const Media = (props) => {
    const [modalVisible, setModalVisible] = useState(false)
    const [selected, setSelected] = useState()
    const [media, setMedia] = useState()
    const [data, setdata] = useState()
    const [video, setVideo] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            console.log('token--', JSON.parse(response))
            Api.getmedia(JSON.parse(response), props.id).then((response) => {
                console.log('media respone---', response)
                if (response.code == 200) {
                    setLoading(false)
                    setdata(response.payload)
                    setMedia(response.payload[0].imageurls)
                    setVideo(response.payload[0].videourls)
                } else {
                    
                }
            })
        })
    }, [])
    function imageModal() {
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible)
                }}>
                <TouchableOpacity style={styles.modalViewContainer}
                    onPressOut={() => setModalVisible(!modalVisible)} activeOpacity={1}>
                    <TouchableWithoutFeedback>
                        <View style={styles.modalView}>
                            <FastImage source={{ uri:selected ? Images.Images.http + selected.split("/")[3] : null, priority: FastImage.priority.high }} style={styles.modalImage} resizeMode={FastImage.resizeMode.cover} />
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            </Modal>
        )
    }
    return (
        <BaseScreen>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                <View style={{ flex: 1, padding: 2.5 }}>
                    {data && data.length == 0 ? 
                      
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text>{strings('Media not found')}</Text>
                  </View>
                  :
                    <ScrollView>
                        
                        <View style={{ padding: 10, alignItems: 'center' }}>
                            <Text style={{ fontSize: Dimensions.FontSize.huge }}>{strings('Images')}</Text>
                        </View>
                        <FlatList
                            scrollEnabled={false}
                            contentContainerStyle={{}}
                            showsVerticalScrollIndicator={false}
                            data={media}
                            extraData={media}
                            renderItem={({ item, index }) => <RenderView data={item} setvisible={(val, image) => { setModalVisible(val), setSelected(image) }} />}
                            keyExtractor={item => item.id}
                            numColumns={2}
                        />
                        <View style={{ padding: 10, alignItems: 'center' }}>
                            <Text style={{ fontSize: Dimensions.FontSize.huge }}>{strings('Videos')}</Text>
                        </View>
                        <FlatList
                            scrollEnabled={false}
                            showsVerticalScrollIndicator={false}
                            data={video}
                            extraData={video}
                            renderItem={({ item, index }) => <RenderVideoView data={item} index={index}/>}
                            keyExtractor={item => item.id}
                        />
                       
                    </ScrollView>
                   }
                    {imageModal()}
                </View>
             } 
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: Dimensions.moderateScale(200),
        borderRadius: 5,
    },
    viewContainer: {
        width: '100%',
        paddingVertical: Dimensions.moderateScale(15),
        paddingHorizontal: Dimensions.moderateScale(10),
        justifyContent: 'center',
        borderRadius: Dimensions.moderateScale(4),
        backgroundColor: '#f6f7fb',
        elevation: 3,
        shadowColor: Color.GreyMedium,
        shadowRadius: 4,
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 2
        }
    },
    textStyle: {
        fontSize: Dimensions.FontSize.large,
        color: '#5286d5',
        textDecorationLine:'underline'
    },
    modalViewContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    modalView: {
        backgroundColor: Color.White,
        alignSelf: 'center',
        width: '80%',
        elevation: 2,
        height: '50%',
        borderRadius: 10,
        overflow: 'hidden'
    },
    modalImage: {
        width: '100%',
        height: '100%',
        borderRadius: 10
    }
});

export default Media;
