import React, { useEffect, useState } from 'react';
import {
    StyleSheet, ActivityIndicator, FlatList,
    Text, Linking,
    View, TouchableOpacity,
    ImageBackground,
    Dimensions as Dm
} from 'react-native';
import Images from '../../../../assets'
import { BaseScreen, Logo, Button } from '../../../../components'
import { WebView } from 'react-native-webview';

const Cer = (props) => {
    return (
        <BaseScreen>
            <View style={{ flex: 1 }}>
            <WebView source={{ uri:`https://drive.google.com/viewerng/viewer?embedded=true&url=${Images.Images.http + props.certificate.split("/")[3]}` }} style={styles.pdf} />
            </View>
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    pdf: {
        flex: 1,
        width: Dm.get('window').width,
        height: Dm.get('window').height,
    }

});

export default Cer;