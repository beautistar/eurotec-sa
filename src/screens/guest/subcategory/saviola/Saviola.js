import React, { useEffect, useState } from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList, ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../../assets'
import { Color, Dimensions } from '../../../../helper'
import { BaseScreen, Logo, Button, ProductColor } from '../../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import Api from '../../../../services/AppApi'
import { strings } from '../../../../languages/I18n';

const Saviola1 = (props) => {
    console.log('saviola props', props)
    const [saviola, setSaviola] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getproducts(props.id).then((response) => {
            console.log('saviola respone---', response)
            if (response.code == 200) {
                setSaviola(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    return (
        <BaseScreen technical={true} tecOnPress={() => { }}>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                saviola && saviola.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <View style={styles.containner}>
                        <ProductColor data={saviola} />
                    </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
});

export default Saviola1;
