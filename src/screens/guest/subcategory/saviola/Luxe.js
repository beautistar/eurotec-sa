import React,{useState,useEffect} from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList,ActivityIndicator,
    Dimensions as dm
} from 'react-native';
import Images from '../../../../assets'
import { Color, Dimensions } from '../../../../helper'
import { BaseScreen, Logo, Button,ProductColor } from '../../../../components'
import { Actions } from 'react-native-router-flux';
import Api from '../../../../services/AppApi'
import { strings } from '../../../../languages/I18n';

const Luxe = (props) => {
    console.log('luxe props----', props)
    const [luxe, setLuxe] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getsubproducts(props.id).then((response) => {
            console.log('luxe respone---', response)
            if (response.code == 200) {
                setLuxe(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    return (
        <BaseScreen technical={true} tecOnPress={() => { }}>
             {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                luxe && luxe.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
            <View style={styles.containner}>
                <ProductColor data={luxe} isSubProduct={props.isSubProduct} />
            </View>
}
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
});

export default Luxe;
