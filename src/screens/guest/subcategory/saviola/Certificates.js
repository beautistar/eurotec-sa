import React, { useEffect, useState } from 'react';
import {
    StyleSheet, ActivityIndicator, FlatList, Text, View, TouchableOpacity, Dimensions as Dm
} from 'react-native';
import Images from '../../../../assets'
import { Color, Dimensions } from '../../../../helper'
import { BaseScreen, Logo, Button } from '../../../../components'
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../../../../services/AppApi'
import { Icon } from 'native-base';
import { strings } from '../../../../languages/I18n';
const DATA = [
    {
        name: 'Blum',
        image: Images.Images.Blum,
    },
    {
        name: 'Vibo',
        image: Images.Images.AlvicBG
    },
    {
        name: 'System Holz',
        image: Images.Images.Blum
    },
    {
        name: 'Car',
        image: Images.Images.AlvicBG
    },
    {
        name: 'Opes Save',
        image: Images.Images.Blum
    }
]
const RenderView = (props) => {
    let { data } = props;
    return (
        <View style={{ paddingHorizontal: 5, paddingVertical: 5 }}>
            <TouchableOpacity style={styles.viewContainer} onPress={() => { Actions.Cer({ certificate: data.document }) }} activeOpacity={0.8}>
                <Icon name='certificate' type='MaterialCommunityIcons' style={{}} />
                <Text style={styles.textStyle}>{data.name}</Text>
            </TouchableOpacity>
        </View>
    )
}
const Certificates = (props) => {
    const [certificate, setCertificate] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            Api.getcertificates(JSON.parse(response), props.id).then((response) => {
                console.log('certificate respone---', response)
                if (response.code == 200) {
                    setCertificate(response.payload)
                    setLoading(false)
                } else {

                }
            })
        })
    }, [])
    return (
        <BaseScreen>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                certificate && certificate.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('Certificates not found')}</Text>
                    </View>
                    :
                    <View style={{ flex: 1, padding: 2.5 }}>
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={certificate}
                            extraData={certificate}
                            renderItem={({ item, index }) => <RenderView data={item} />}
                            keyExtractor={item => item.id}
                        />
                    </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        width: '100%',
        paddingVertical: Dimensions.moderateScale(15),
        paddingHorizontal: Dimensions.moderateScale(10),
        borderRadius: Dimensions.moderateScale(4),
        flexDirection: 'row',
        backgroundColor: '#f6f7fb',
        elevation: 3,
        shadowColor: Color.GreyMedium,
        shadowRadius: 4,
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 2
        }
    },
    textStyle: {
        fontSize: Dimensions.FontSize.large,
        color: Color.Black,
        paddingLeft: Dimensions.moderateScale(10)
    },
});

export default Certificates;