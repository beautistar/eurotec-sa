import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text, ImageBackground,
    View, TouchableOpacity, ActivityIndicator,
    Image, FlatList
} from 'react-native';
import Images from '../../../../assets'
import { Color, Dimensions } from '../../../../helper'
import { BaseScreen, Logo, Button } from '../../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../../../../services/AppApi'
import { strings } from '../../../../languages/I18n';
const DATA = [
    {
        name: 'Blum',
        image: Images.Images.Blum
    },
    {
        name: 'Vibo',
        image: Images.Images.AlvicBG
    },
    {
        name: 'System Holz',
        image: Images.Images.Blum
    },
    {
        name: 'Car',
        image: Images.Images.AlvicBG
    },
    {
        name: 'Opes Save',
        image: Images.Images.Blum
    }
]
const RenderView = (props) => {
    let { data } = props;
    return (
        <View style={{ paddingHorizontal: 5, paddingVertical: 2.5 }}>
            <FastImage source={{ uri: Images.Images.http + data.image.split("/")[3], priority: FastImage.priority.high }} style={styles.image}>
                <View style={styles.viewContainer}>
                    <Text style={styles.textStyle}>{data.name}</Text>
                    {/* <TouchableOpacity onPress={() => {}}
                        style={styles.buttonContainer}>
                        <Text style={styles.buttonText}>Discover More</Text>
                    </TouchableOpacity> */}
                </View>
            </FastImage>
        </View>
    )
}
const Accessories = (props) => {
    console.log('accee props---', props)
    const [products, setProducts] = useState([])
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            console.log('token--', JSON.parse(response))
            Api.getproducts(JSON.parse(response), props.id).then((response) => {
                console.log('product respone---', response)
                if (response.code == 200) {
                    setProducts(response.payload)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        })
    }, [])
    return (
        <BaseScreen>
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                products && products.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('Products not found')}</Text>
                    </View>
                    :
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        style={{ flex: 1 }}
                        data={products}
                        extraData={products}
                        renderItem={({ item, index }) => <RenderView data={item} />}
                        keyExtractor={item => item.id}
                    />
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: Dimensions.moderateScale(280),
    },
    viewContainer: {
        width: '100%',
        height: Dimensions.moderateScale(280),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.3)',
    },
    textStyle: {
        fontSize: Dimensions.FontSize.extraHuge,
        color: Color.White
    },
    buttonContainer: {
        borderColor: Color.White,
        borderWidth: 2,
        padding: Dimensions.moderateScale(10),
        marginTop: Dimensions.moderateScale(15)
    },
    buttonText: {
        fontSize: Dimensions.FontSize.large,
        color: Color.White
    }
});

export default Accessories;
