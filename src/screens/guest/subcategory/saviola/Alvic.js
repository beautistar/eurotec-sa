import React, { useState, useEffect } from 'react';
import {
    Platform,
    StyleSheet,
    FlatList,
    View, TouchableOpacity,
    Image, ActivityIndicator, Text,
    Dimensions as dm
} from 'react-native';
import Images from '../../../../assets'
import { Color, Dimensions } from '../../../../helper'
import { BaseScreen, Logo, Button } from '../../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import Api from '../../../../services/AppApi'
import { strings } from '../../../../languages/I18n';

const RenderView = (props) => {
    let { data } = props;
    return (
        // <View style={{ height: (dm.get('window').height - ((dm.get('window').height - 56) / 5)) / 2.2, }}>
        <View style={{ height: props.dheight ? props.dheight / 2 : '100%' }}>
            <TouchableOpacity style={styles.viewContainer}
                activeOpacity={0.8} onPress={() => { Actions.Luxe({ id: data._id, title: data.name.toUpperCase(), isSubProduct: data.isSubProduct }) }}>
                <FastImage source={{ uri: data.image, priority: FastImage.priority.high }} resizeMode={FastImage.resizeMode.stretch} style={styles.image} />
            </TouchableOpacity>
        </View>
        // </View>
    )
}

const Alvic = (props) => {
    console.log('alvic props', props)
    const [alvic, setAlvic] = useState()
    const [dheight, setdHeight] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Api.getproducts(props.id).then((response) => {
            console.log('alvic respone---', response)
            if (response.code == 200) {
                setAlvic(response.payload)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])
    if (loading)
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
        )
    else
        return (
            <BaseScreen technical={true} tecOnPress={() => { }}>
                {alvic && alvic.length == 0 ?
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>{strings('No Data Found')}</Text>
                    </View>
                    :
                    <View onLayout={(e) => setdHeight(e.nativeEvent.layout.height)} style={{ flex: 1,}}>
                        <FlatList
                            contentContainerStyle={{}}
                            showsVerticalScrollIndicator={false}
                            data={alvic}
                            extraData={alvic}
                            renderItem={({ item, index }) => <RenderView data={item} dheight={dheight} />}
                        // keyExtractor={item => item.id}
                        />
                    </View>
                }
            </BaseScreen>
        );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        flex: 1,
        // flexDirection: 'row',
    },
    image: {
        height: '100%',
        width: '100%',
    },
});

export default Alvic;
