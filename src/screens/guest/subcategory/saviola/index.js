import React from 'react';
import {
    StyleSheet,
    Text,
    View, TouchableOpacity, Linking,
    Image, ImageBackground
} from 'react-native';
import Images from '../../../../assets'
import { Color, Dimensions } from '../../../../helper'
import { BaseScreen, Logo, Button, } from '../../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import { strings } from '../../../../languages/I18n';
const RenderItem = (props) => {
    return (
        <TouchableOpacity style={styles.imageView} onPress={props.onPress} activeOpacity={0.8}>
            <FastImage source={{ uri: props.image, priority: FastImage.priority.high }}
                style={{ width: '100%', height: '100%' }} resizeMode={props.resizeMode}>
                <Text style={[styles.textStyle, props.textProps]}>{props.name}</Text>
            </FastImage>
        </TouchableOpacity>
    )
}
const Saviola = (props) => {
    console.log('props---', props)
    return (
        <BaseScreen>
            <View style={styles.containner}>
                <View style={{ height: '70%', }}>
                    <View style={styles.mainView}>
                        <RenderItem
                            name={strings('MEDIA')}
                            image={Images.Images.Media}
                            resizeMode={FastImage.resizeMode.cover}
                            onPress={() => { Actions.Media({ title: strings('MEDIA'), id: props.data._id }) }} />
                        <RenderItem image={Images.Images.History} name={strings('HISTORY')}
                            textProps={{ right: 0, }}
                            resizeMode={FastImage.resizeMode.cover}
                            onPress={() => { Actions.History({ title: strings('HISTORY'), history: props.data.historydescription }) }} />
                    </View>
                    <View style={styles.mainView}>
                        <RenderItem
                            name={strings('PRODUCTS')}
                            image={Images.Images.Producct}
                            resizeMode={FastImage.resizeMode.cover}
                            onPress={() => { Actions.Products({ title: strings('PRODUCT'), id: props.data._id }) }} />
                        <RenderItem image={Images.Images.Certificate}
                            name={strings('CERTIFICATES')}
                            textProps={styles.cerTextStyle}
                            resizeMode={FastImage.resizeMode.contain}
                            onPress={() => { Actions.Certificates({ title: strings('CERTIFICATES'), id: props.data._id }) }} />
                    </View>
                </View>
                <TouchableOpacity style={styles.tecView} activeOpacity={0.8}
                    onPress={() => { Linking.openURL('http://www.eurotec.sa.com/services/#technical_support') }}>
                    <FastImage source={{ uri: Images.Images.TechSupport, priority: FastImage.priority.high }} style={{ width: '100%', height: '100%', }} resizeMode={FastImage.resizeMode.stretch} />
                </TouchableOpacity>
            </View>
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        padding: Dimensions.moderateScale(2.5)
    },
    mainView: {
        height: '50%',
        width: '100%',
        alignItems: "center",
        flexDirection: 'row',
    },
    imageView: {
        width: '50%',
        height: '100%',
        paddingHorizontal: Dimensions.moderateScale(2.5),
        paddingVertical: Dimensions.moderateScale(2.5)
    },
    textStyle: {
        fontWeight: 'bold',
        fontSize: Dimensions.moderateScale(25),
        color: Color.White,
        position: 'absolute',
        bottom: 0,
        paddingHorizontal: Dimensions.moderateScale(7)
    },
    tecView: {
        height: '30%',
        width: '100%',
        alignItems: 'center',
        padding: Dimensions.moderateScale(2.5)
    },
    cerTextStyle: {
        color: Color.Grey656466,
        right: 0,
        textAlign: 'right',
        fontSize: Dimensions.moderateScale(23.8)
    }
});

export default Saviola;
