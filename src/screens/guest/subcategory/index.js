import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    Dimensions as dm,
    View, TouchableOpacity,ActivityIndicator,
    Image, ScrollView
} from 'react-native';
import Images from '../../../assets'
import { Color, Dimensions } from '../../../helper'
import { BaseScreen, Logo, Button } from '../../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../../../services/AppApi'
import { strings } from '../../../languages/I18n';

const RenderView = (props) => {
    return (
        <View style={{ height:  props.dheight ? props.dheight / 2 : '100%' , flexDirection: 'row' }}>
            <TouchableOpacity activeOpacity={0.8} onPress={props.onPress}
                style={[props.imageview, { width: "100%",  }]}>
                <FastImage source={{ uri: props.image, priority: FastImage.priority.high }} style={{ width: '100%', height: '100%' }} resizeMode={FastImage.resizeMode.stretch} />
            </TouchableOpacity>
        </View>
    )
}

const Wood = (props) => {
    const [subcat, setSubCat] = useState()
    const [dheight, setdHeight] = useState()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            console.log('token--', JSON.parse(response))
            Api.subcategories(props.id).then((response) => {
                console.log('sub cat respone---', response)
                if (response.code == 200) {
                    setSubCat(response.payload)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        })
    }, [])
    if (loading)
    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator color={Color.orenge} size="large" />
        </View>
    )
else
    return (
        <BaseScreen>
               { subcat && subcat.length == 0 ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{strings('Subcategories not found')}</Text>
                </View>
                :
                <View onLayout={(e) => setdHeight(e.nativeEvent.layout.height)} style={styles.containner}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {subcat.map((item, index) =>
                            <RenderView
                                onPress={() => {item.name == 'Saviola' || item.name == 'saviola' ? Actions.Saviola1({id: item._id, title: item.name.toUpperCase()}) : Actions.Alvic({id: item._id, title: item.name.toUpperCase()}) }}
                                image={item.image}
                                desc={item.description}
                                dheight={dheight}
                                />
                        )}
                    </ScrollView>
                </View>
            }
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
});

export default Wood;
