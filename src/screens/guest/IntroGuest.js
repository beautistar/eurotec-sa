import React from 'react';
import {Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button } from '../../components'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import { strings } from '../../languages/I18n';

    const IntroGuest = () => {
    return (
        <BaseScreen>
            <FastImage source={{ uri: Images.Images.who, priority: FastImage.priority.high }}
                style={{ height: Dimensions.moderateScale(250), width: '100%',borderColor:Color.Grey5e5e5e,borderWidth:2.5 }}
                resizeMode={FastImage.resizeMode.cover} />
            <View style={{paddingBottom:Platform.OS == 'ios' ? '25%' : 0}}>
                <ScrollView contentContainerStyle={{paddingBottom:Platform.OS == 'ios' ? '20%' : '60%' }}>
                    <Text style={styles.headerText}>{strings('Who we are')}</Text>
                    <Text style={styles.textContain}>{strings('Who we are contain')}</Text>
                </ScrollView>
            </View>
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        height: '20%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: Dimensions.FontSize.extraHuge,
        color: Color.Grey848385,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: Dimensions.moderateScale(5)
    },
    textContain: {
        fontSize: Dimensions.FontSize.extraLarge,
        paddingHorizontal: Dimensions.moderateScale(20),
        color: Color.Grey848385,
        textAlign:'justify',
        lineHeight: Dimensions.moderateScale(27)
    }
});

export default IntroGuest;
