import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    View, TouchableOpacity,ScrollView,
    Image, FlatList, ImageBackground, ActivityIndicator
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button } from '../../components'
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import Api from '../../services/AppApi'
import FastImage from 'react-native-fast-image'
import { strings } from '../../languages/I18n';

const RenderView = (props) => {
    var { data } = props;
    return (
        <View style={styles.cardView}>
            <FastImage source={{ uri: Images.Images.http + data.image.split("/")[3], priority: FastImage.priority.high }} style={styles.image} />
            <Text style={styles.titleText}>{data.title}</Text>
            <Text style={styles.typeText}>{data.username.firstName+ ' ' + data.username.lastName + ' | ' + data.categoryname.name}</Text>
            <Text style={styles.descText}>{data.description}</Text>
        </View>
    )
}
const News = () => {
    const [token, settoken] = useState()
    const [page, setpage] = useState(1)
    const [events, setEvents] = useState()
    const [loading, setLoading] = useState(true)
    async function handleLoadMore(e) {
        await setpage(page + 1)
        eventget()
    }
    function eventget(data, token) {
        AsyncStorage.getItem("token").then((response) => {
            Api.events(page, JSON.parse(response)).then((response) => {
                console.log('event response---', response)
                if (response.code == 200) {
                    var data = page == 1 ? response.payload.eventList : [...events, ...response.payload.eventList]
                    setEvents(data)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        })
    }
    useEffect(() => {
        eventget()
    }, [])
    return (

        <BaseScreen>
            {/* {console.log('ev-----', events)} */}
            {loading ? <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator color={Color.orenge} size="large" />
            </View>
                :
                events?
                <FlatList showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ padding: Dimensions.moderateScale(2.5) }}
                    style={{ flex: 1, paddingVertical: Dimensions.moderateScale(2.5), }}
                    data={events}
                    renderItem={({ item, index }) => <RenderView data={item} />}
                    keyExtractor={item => item.id}
                    onEndReached={(e) => handleLoadMore(e)}
                /> 
                : <View style={{ flex: 1, justifyContent: 'center',alignItems:'center' }}>
                <Text>{strings('No Data Found')}</Text>
            </View>
            }
            
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: Dimensions.moderateScale(230),
        elevation: 2
    },
    cardView: {
        paddingHorizontal: Dimensions.moderateScale(5),
        paddingVertical: Dimensions.moderateScale(2.5),
        elevation: 5,
        marginBottom: Dimensions.moderateScale(10)
    },
    titleText: {
        fontSize: Dimensions.FontSize.extraLarge,
        color: Color.orenge,
        fontWeight: 'bold',
        marginTop: Dimensions.moderateScale(10)
    },
    typeText: {
        fontSize: Dimensions.FontSize.tiny,
        color: Color.Grey656466,
        marginTop: Dimensions.moderateScale(10)
    },
    descText: {
        fontSize: Dimensions.FontSize.medium,
        color: Color.Grey656466,
        marginTop: Dimensions.moderateScale(10),
        textAlign: 'justify',
        lineHeight: Dimensions.moderateScale(20)
    }
});

export default News;
