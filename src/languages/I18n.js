import React, { useEffect, useState } from 'react';
import ReactNative from 'react-native';
import I18n from 'react-native-i18n';
import AsyncStorage from '@react-native-community/async-storage';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
    'English': require('../languages/English.json'),
    'Arabic': require('../languages/Arabic.json')
}
// const currentLocale = I18n.currentLocale();

// Is it a RTL language?
// export const isRTL = currentLocale.indexOf('he') === 0 || currentLocale.indexOf('ar') === 0;

export var isRTL = false
// Allow RTL alignment in RTL languages
// ReactNative.I18nManager.allowRTL(isRTL);
export async function isrtl() {
    var select = await AsyncStorage.getItem("Language").then((res) => {
        if (res == 'Arabic') {
            isRTL = true
        } else {
            isRTL = false
        }
    })
}
// The method we'll use instead of a regular string
export  function strings(name, params = {}) {
    var l 
    // const [language, setLanguage] = useState()
     AsyncStorage.getItem("Language").then((response) => {
        console.log('lang--', response)
        l = response
        I18n.locale = response
        // setLanguage(response)
    })
    // I18n.locale = l
    return I18n.t(name, params);
};

export default I18n;