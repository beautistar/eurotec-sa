class AppApi {
    static headers(token) {
      return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'authorization': token,
        'dataType': 'json',
      }
    }
  
    static eurotec(route, params, token, verb, ) {
      // console.log('Api token----',token)
      const host = 'http://142.93.97.198/rest/'
      const url = `${host}${route}`
      let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null);
      options.headers = AppApi.headers(token)
      return fetch(url, options).then(resp => {
        // console.log('resp====',resp)
        let json = resp.json();
        if (resp.ok) {
          return json;
        }else{
        return json.then(err => err);
        }
      }).then(json => json)
      .catch((e) => {
        console.log('error----',e)
        return e
      });
    }
    static login(params) {
      return this.eurotec('auth/login', params, null, 'POST')
    }
    static register(params) {
      return this.eurotec('auth/register', params, null, 'POST')
    }
    static forgot(params) {
      return this.eurotec('auth/forgotpassword', params, null, 'POST')
    }
    static changepassword(params,token) {
      return this.eurotec('auth/changePassword', params, token, 'POST')
    }
    static events(params,token) {
      console.log('events dattaaa------',params,token)
      return this.eurotec('event/getAll?pageNo='+params+'&limit='+3, null, null, 'GET')
    }
    static contact(token) {
      return this.eurotec('contact/getAll', null, null, 'GET')
    }
    static categories(token) {
      return this.eurotec('category/getAll', null, null, 'GET')
    }
    static subcategories(id) {
      console.log('dattaaa------',id)
      return this.eurotec(`subCategory/getByCategory/${id}`, null, null, 'GET')
    }
    static getproducts(id) {
      return this.eurotec(`product/getBySubCategory/${id}`, null, null, 'GET')
    }
    static getsubproducts(id) {
      return this.eurotec(`subProduct/getByProduct/${id}`, null, null, 'GET')
    }
    // static getcertificates(token,id) {
    //   return this.eurotec(`certificate/getBySubCategory/${id}`, null, null, 'GET')
    // }
    // static getmedia(token,id) {
    //   return this.eurotec(`media/getBySubCategory/${id}`, null, null, 'GET')
    // }

    static getfilesbyproduct(id) {
      return this.eurotec(`productFiles/getByProduct/${id}`, null, null, 'GET')
    }
    static getfilesbysubproduct(id) {
      return this.eurotec(`productFiles/getBySubProduct/${id}`, null, null, 'GET')
    }
  }
  
  export default AppApi
  