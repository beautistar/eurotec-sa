import BaseScreen from "./general/BaseScreen";
import NavigationBar from './general/NavigationBar';
import Logo from './general/Logo';
import Button from './general/Button';
import TextInput from './general/TextInput';
import ImageGallery from './general/ImageGallery';
// import ProductView from './wood/View';
import File from './general/File'
import ProductColor from './wood/Color';
export {
    BaseScreen,
    NavigationBar,
    Logo,
    Button,
    TextInput,
    ImageGallery,
    // ProductView,
    File,
    ProductColor
}