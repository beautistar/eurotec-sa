import React from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList,
    Dimensions as dm
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'

const RenderView = (props) => {
    let { data } = props;
    return (
        <TouchableOpacity style={styles.viewContainer}
            activeOpacity={0.8} onPress={() => { Actions.AlvicCer({ title: data.name.toUpperCase(), id: data._id, isSubProduct: props.isSubProduct ? props.isSubProduct : data.isSubProduct }) }}>
            <FastImage source={{ uri: data.image, priority: FastImage.priority.high }} resizeMode={FastImage.resizeMode.stretch} style={styles.image} />
        </TouchableOpacity>
    )
}
const ProductColor = (props) => {
    return (
        <FlatList
            contentContainerStyle={{}}
            showsVerticalScrollIndicator={false}
            data={props.data}
            extraData={props.data}
            renderItem={({ item, index }) => <RenderView data={item} isSubProduct={props.isSubProduct}/>}
            keyExtractor={item => item.id}
            numColumns={2}
        />
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        // flex: 1, 
        flexDirection: 'row',
        width: '50%',
        height: (dm.get('window').height - ((dm.get('window').height - 56) / 5)) / 3.6
    },
    image: {
        width: '100%',
    },
});

export default ProductColor;
