import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Image, TouchableOpacity,
    Dimensions, ActivityIndicator
} from 'react-native';
import { Actions } from 'react-native-router-flux';

const GalleryImage = (props) => {
    console.log('gallery props------', props)
    const { image, name, _id, isSubProduct } = props.items
    const [height, setHeight] = useState(0)
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        Image.getSize(image, (width, height) => {
            // setHeight(height)
            setHeight(height * (Dimensions.get('window').width / (width * 1.5)))
        }, (err) => console.log('Image error', err))
    }, [])
    function startLoad() {
        setLoading(true)
    }
    function finishLoad() {
        setLoading(false)
    }
    return (
        <TouchableOpacity style={styles.imageContainer} activeOpacity={0.9}
            onPress={() => { Actions.AventosFiles({ title: name.toUpperCase(), id: _id, isSubProduct: isSubProduct }) }}>
            <Image onLoadStart={() => startLoad()} onLoad={() => finishLoad()} resizeMode='stretch' source={{ uri: image }} style={{ height: height }} />
            {loading ?
                <View style={{ position: 'absolute', top: height / 2, alignSelf: 'center' }}>
                    <ActivityIndicator size={'large'} />
                </View> : null}
        </TouchableOpacity>
    )
}

const ImageGallery = (props) => {
    const { data } = props
    const [column1, setColumn1] = useState([])
    const [column2, setColumn2] = useState([])
    useEffect(() => {
        var col1 = []
        var col2 = []
        if (data) {
            data.map((item, index) => {
                if (index % 2 == 0) {
                    col1.push(item)
                } else {
                    col2.push(item)
                }
            })
        }
        setColumn1(col1)
        setColumn2(col2)
    }, [])
    return (
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'white' }}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ alignItems: 'center' }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '50%', }}>
                        {column2.map((item, index) =>
                            <GalleryImage items={item} />
                        )}
                    </View>
                    <View style={{ width: '50%', }}>
                        {column1.map((item, index) =>
                            <GalleryImage items={item} />
                        )}
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    imageContainer: {
        // elevation: 2.5,
        // margin: 0.25,
        backgroundColor: 'white',
    }
});

export default ImageGallery;
