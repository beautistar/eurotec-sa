import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import FastImage from 'react-native-fast-image'
const Logo = () => {
  return (
    <View style={{alignSelf:'center'}}>
      <View>
        <Image source={Images.Images.logo} resizeMode='contain' style={styles.image}  />
        {/* <FastImage source={{uri:Images.Images.logo,priority:FastImage.priority.high}} style={styles.image}  resizeMode={FastImage.resizeMode.contain} /> */}
      </View>
      <Text style={styles.textStyle}>HIGH END TECHNOLOGY TRADING EST</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  containner: {
    flex: 1,
    justifyContent: 'center'
  },
  image:{
    width: '100%', 
    height: Dimensions.moderateScale(70) 
  },
  textStyle:{
    fontSize: Dimensions.FontSize.large, 
    color: Color.Gray656466, 
  }
});

export default Logo;