import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View, TouchableOpacity, ScrollView,
    Image
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { BaseScreen, Logo, Button } from '../../components'
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { strings,isRTL,isrtl } from '../../languages/I18n';
const RenderView = (props) => {
    return (
        <View>
            <TouchableOpacity onPress={props.onPress} style={[styles.itemsView,{paddingRight: isRTL ? Dimensions.moderateScale(10) : 0,}]} >
                <Text style={[styles.textStyle,{paddingRight: isRTL ? Dimensions.moderateScale(20) : 0,textAlign: isRTL ? 'right' : 'left'}]}>{strings(props.name)}</Text>
            </TouchableOpacity>
        </View>
    )
}
const SideBar = () => {
    isrtl()
    const [token, settoken] = useState()
    const [user, setuser] = useState()
    var keysignin = strings('Sign-In')
    var keylogout = strings('Logout')
    var contact = strings('Contact')
    var news = strings('News & Events')
    var changepassword = strings('CHANGE PASSWORD')
    var whoweare = strings('Who we are')
    useEffect(() => {
        AsyncStorage.getItem("token").then((response) => {
            settoken(JSON.parse(response))
        })
        AsyncStorage.getItem("login_data").then((response) => {
            setuser(JSON.parse(response))
        })
    }, [])
    function logout() {
        AsyncStorage.clear(),
            Actions.reset('Welcome')
    }
    return (
        <BaseScreen>
            <View style={styles.containner}>
                <View style={styles.topView}>
                    <Text style={{fontSize:20,}}>Welcome,</Text>
                    <Text style={{fontSize:18,}}>{user ? user.userName : null}</Text>
                </View>
                <View style={styles.middleView}>
                    <ScrollView>
                        {token ? null : <RenderView name='Sign-In' onPress={() => Actions.reset('signin1',{title:keysignin.toUpperCase()})} />}
                        <RenderView name='Home' onPress={() => Actions.reset('Guest')} />
                        <RenderView name="News & Events" onPress={() => Actions.reset('News',{title:news.toUpperCase()})} />
                        <RenderView name='Contact' onPress={() => Actions.reset('Contact',{title:contact.toUpperCase()})} />
                        {token ? <RenderView name='Change Password' onPress={() => Actions.reset('ChangePassword',{title:changepassword})} /> : null}
                    </ScrollView>
                </View>
                {token ? <View style={styles.bottomView}>
                    <TouchableOpacity style={[styles.logoutView,{paddingRight: isRTL ? Dimensions.moderateScale(10) : 0}]} onPress={() => logout()}>
                        <Text style={[styles.textStyle,{paddingRight: isRTL ? Dimensions.moderateScale(20) : 0,textAlign: isRTL ? 'right' : 'left'}]}>{keylogout}</Text>
                    </TouchableOpacity>
                </View> : null}
            </View>
        </BaseScreen>
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
        backgroundColor: '#fff',
    },
    itemsView: {
        borderBottomWidth: 2,
        borderColor: '#f2f3f5',
        paddingVertical: Dimensions.moderateScale(15),
        paddingLeft: Dimensions.moderateScale(10)
    },
    textStyle: {
        paddingLeft: Dimensions.moderateScale(20),
        fontSize: Dimensions.FontSize.medium,
        fontWeight: 'bold',
        color: Color.Grey656466
    },
    logoutView: {
        paddingVertical: Dimensions.moderateScale(15),
        borderTopWidth: 2,
        borderColor: '#f2f3f5',
        paddingLeft: Dimensions.moderateScale(10)
    },
    topView: {
        flex: 0.2,
        backgroundColor: Color.orenge,
        justifyContent:'flex-end',
        padding:10,
    },
    middleView: {
        flex: 0.7,
        padding: Dimensions.moderateScale(10),
    },
    bottomView: {
        flex: 0.1,
        paddingHorizontal: Dimensions.moderateScale(10)
    }
});

export default SideBar;
