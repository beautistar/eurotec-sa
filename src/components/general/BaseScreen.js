import React from 'react';

import {
  View,
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  Platform,
  Dimensions as dm
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import FastImage from 'react-native-fast-image'
const BaseScreen = (props) => {
  return props.backgroundImage ?
    (
      <ImageBackground
        source={props.backgroundImage}
        resizeMode={props.resizeMode ? props.resizeMode : 'stretch'}
        style={styles.container}>
        <StatusBar backgroundColor={Color.Greye1e0e2} barStyle="dark-content" />
        {props.children}
      </ImageBackground>
    ) :
    (
      <View
        style={styles.container}>
        <StatusBar backgroundColor={Color.Greye1e0e2} barStyle="dark-content" />
        {props.children}
        {props.technical ?
        <View style={{ height: (dm.get('window').height-56) / 5,borderWidth:2,borderColor:Color.GreyMedium }}>
          <TouchableOpacity activeOpacity={0.8} onPress={props.tecOnPress}
            style={{width: '100%',}}>
            <FastImage source={{ uri: Images.Images.url + 'Technical.jpg', priority: FastImage.priority.high }} 
            resizeMode={FastImage.resizeMode.stretch} 
            style={{ height: '100%', width: '100%', }}>
            </FastImage>
          </TouchableOpacity>
        </View>
        :null }
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: 'hidden',
    backgroundColor: Color.White,
  },
});

export default BaseScreen;