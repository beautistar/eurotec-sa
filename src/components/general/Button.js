import React from 'react';
import {
    StyleSheet,
    Text, TouchableOpacity,ActivityIndicator
} from 'react-native';
import { Color, Dimensions } from '../../helper'
import { strings } from '../../languages/I18n';

const Button = (props) => {
    return (
        <TouchableOpacity disabled={props.disabled}
            activeOpacity={0.8}
            style={[styles.container, props.containerStyle]}
            onPress={props.onPress}>
                 {props.title == 'loading' ?  <ActivityIndicator color="white" size="large" />
                 : <Text style={[styles.textStyle, props.textStyle]}>{strings(props.title).toUpperCase()}</Text> }
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Color.Grey5e5e5e, 
        // width: '45%',
        height: Dimensions.moderateScale(50),
        borderWidth: Dimensions.moderateScale(3),
        borderColor: Color.orenge,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: Dimensions.FontSize.extraLarge,
        color: Color.orenge,
        fontWeight: 'bold'
    }
});

export default Button;
