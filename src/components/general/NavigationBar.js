import React from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet, Platform
} from 'react-native';
import {Icon} from 'native-base'
import { Color, Dimensions,IPhoneXhelper} from '../../helper';

export default class NavigationBar extends React.Component{

  render() {
    let { title, goBack, onRight, rightTitle, noMenu, titleStyle, rightIcon, lefttitle = null,maintitleStyle } = this.props;
    if (noMenu) return null;
    return (
      <View style={styles.headerContainer}>
        {goBack ? <TouchableOpacity onPress={() => goBack?goBack():null} style={styles.left}>
          {lefttitle?<Text style={styles.rightText}>{lefttitle} </Text>:<Icon name='arrowleft' type='AntDesign' style={styles.iconStyle}/>}

        </TouchableOpacity> : <View style={styles.left}></View>}
        <View style={[styles.titleContainer,maintitleStyle]}>
          <Text style={[styles.title, titleStyle]}>{title}</Text>
        </View>
        {onRight != null && rightTitle != '' ?
        <TouchableOpacity
          onPress={() => onRight?onRight():null}
          style={styles.right}>
         {rightIcon ? rightIcon : <Text style={styles.rightText}>{rightTitle} </Text>}
        </TouchableOpacity> : <View style={styles.right}></View>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    height: Dimensions.NavBar.height + IPhoneXhelper.getStatusBarHeight(false),
    paddingTop: IPhoneXhelper.getStatusBarHeight(false),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingHorizontal: 10
  },

  title: {
    fontSize: 16,
    //fontWeight: 'bold',
    color: 'white',
    letterSpacing: 1
  },


  titleContainer: {
    // flex: 1,
    position: 'absolute',
    left: 50,
    right: 50,
    top: Dimensions.IPhoneXhelper.getStatusBarHeight(false),
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30
    // marginHorizontal: 40,
  },
  leftContainer: {
    flex: 1
  },
  rightContainer: {
    flex: 1
  },
  left: {
    padding: 10,
    marginLeft: 5
    // paddingVertical: 8,
    // paddingHorizontal: 20,
    // position: 'absolute',
    // bottom: 0
  },
  right: {
    padding: 10,
    marginRight: 10
    // position: 'absolute',
    // paddingVertical: 8,
    // paddingHorizontal: 20,
    // bottom: Platform.OS == 'ios' ? 14 : 8,
    // right: 0
  },
  rightText: {
    fontSize: 12,
    color: Color.White,
  },
  dummyView: {
    padding: 10
  },
  iconStyle: {
    color: Color.White,
    fontSize: 25
},
});
