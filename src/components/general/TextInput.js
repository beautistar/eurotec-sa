import React from 'react';
import {
    StyleSheet,
    TextInput
} from 'react-native';
import { Color, Dimensions } from '../../helper'
import { strings,isRTL,isrtl } from '../../languages/I18n';

const Textinput = (props) => {
    isrtl()
    return (
        <TextInput
            style={[styles.textInput, props.textProps,{textAlign: isRTL ? 'right' : 'left',paddingRight : isRTL ? Dimensions.moderateScale(8) : 0}]}
            secureTextEntry={props.secureTextEntry}
            // placeholderTextColor={Color.GreyMedium}
            placeholder={props.labelValue}
            onChangeText={props.onChangeText}
            value={props.value}
            keyboardType={props.keyboardType}
            autoCapitalize='none'
            autoCorrect={false}
            maxLength={props.maxLength}
            numberOfLines={props.numberOfLines}
            onSubmitEditing={props.onSubmitEditing}
            editable={props.editable}
            multiline={props.multiline}
            onEndEditing={props.onEndEditing}
        />
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Color.White,
        // width: '45%',
        height: Dimensions.moderateScale(50),
        borderWidth: Dimensions.moderateScale(3),
        borderColor: Color.orenge,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textInput: {
        height: Dimensions.moderateScale(45),
        borderColor: Color.orenge, 
        borderWidth: Dimensions.moderateScale(3),
        paddingLeft:Dimensions.moderateScale(8),
    }
});

export default Textinput;
