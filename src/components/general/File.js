import React from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, TouchableOpacity,
    Image,
    FlatList,Linking,
    Dimensions as dm
} from 'react-native';
import Images from '../../assets'
import { Color, Dimensions } from '../../helper'
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image'
import { Icon } from 'native-base';

const RenderView = (props) => {
    let { item } = props;
    console.log('props-------', props)
    return (
        <TouchableOpacity style={styles.viewContainer}
            activeOpacity={0.8} onPress={() => Linking.openURL(item.file)}>
            <View style={{ width: '20%', paddingHorizontal: 5 }}>
                <View style={styles.iconView}>
                    <Icon name='tools' type='Octicons' style={{ fontSize: 25 }} />
                </View>
            </View>
            <View style={{ width: '80%', paddingHorizontal: 5, }}>
                <View style={styles.textContainer}>
                    <Text style={styles.textStyle}>{item.name}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}
const File = (props) => {

    return (
        <FlatList
            contentContainerStyle={{marginTop:Dimensions.moderateScale(10)}}
            showsVerticalScrollIndicator={false}
            data={props.data}
            extraData={props.data}
            renderItem={({ item, index }) => <RenderView item={item} />}
            keyExtractor={item => item.id}
        />
    );
};

const styles = StyleSheet.create({
    containner: {
        flex: 1,
    },
    viewContainer: {
        flex: 1,
        flexDirection: 'row',
        marginVertical: 4
    },
    iconView: {
        borderColor: Color.Grey656466,
        borderWidth: 3,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 2,
        borderRadius: 10,
        height: Dimensions.moderateScale(38)
    },
    textContainer: {
        backgroundColor: Color.Grey656466,
        paddingVertical: 2,
        borderRadius: 10,
        height: Dimensions.moderateScale(38),
        paddingHorizontal: 5,
        elevation: 10,
        shadowColor: Color.Grey656466,
        shadowRadius: 4,
        shadowOpacity: 0.8,
        shadowOffset: {
            width: 0,
            height: 4
        }
    },
    image: {
        width: '100%',
        height: '100%'
    },
    textStyle: {
        fontSize: Dimensions.moderateScale(15),
        color: Color.White
    }
});

export default File;
