/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/navigation';
import App1 from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
